# 大綱

這是一個關於資料處理筆記,並且主要以地球科學（尤其是地球物理學）中所需要進行的一些指令以及作圖程序,內容含括製圖軟體GMT在Shell中的指令
以及python及其科學運算相關的套件與視覺化工具,包含Numpy, Scipy, Matlab Pyplot, Pandas 等。

本課程內容由國立臺灣師範大學地球科學系[林佩瑩](https://scholar.lib.ntnu.edu.tw/en/persons/pei-ying-patty-lin)教授提供。

```{tableofcontents}
```

